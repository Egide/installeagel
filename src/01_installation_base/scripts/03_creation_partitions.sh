# echo -e "\n### Création des partitions sur le SSD : "
# echo -n "# Création partition 1   : " ; sgdisk --new=1:2048:616447 /dev/sde
# echo -n "# renommé partition UEFI : " ; sgdisk --change-name=1:UEFI /dev/sde
# echo -n "# type UEFI              : " ; sgdisk --typecode=1:EF00 /dev/sde
# echo -n "# attribut de boot       : " ; sgdisk --attributes=1:set:2 /dev/sde

# echo -n "# Création partition 2   : " ; sgdisk --new=2:616448:1230847 /dev/sde
# echo -n "# renommé partition BOOT : " ; sgdisk --change-name=2:BOOT /dev/sde
# echo -n "# type GNU/Linux         : " ; sgdisk --typecode=2:8300 /dev/sde

# echo -n "# Création partition 3   : " ; sgdisk --new=3:1230848:234441614 /dev/sde
# echo -n "# renommé partition SYST : " ; sgdisk --change-name=3:SYST /dev/sde
# echo -n "# type GNU/Linux LVM     : " ; sgdisk --typecode=3:8E00 /dev/sde

# echo -e "\n### Création des partitions sur le disque raid a : "
# echo -n "# Création partition 1   : " ; sgdisk --new=1:2048:5860320000 /dev/sda
# echo -n "# renommé partition      : " ; sgdisk --change-name=1:RAID1 /dev/sda
# echo -n "# type GNU/Linux raid    : " ; sgdisk --typecode=1:FD00 /dev/sda

# echo -e "\n### Création des partitions sur le disque raid b : "
# echo -n "# Création partition 1   : " ; sgdisk --new=1:2048:5860320000 /dev/sdb
# echo -n "# renommé partition      : " ; sgdisk --change-name=1:RAID2 /dev/sdb
# echo -n "# type GNU/Linux raid    : " ; sgdisk --typecode=1:FD00 /dev/sdb

# echo -e "\n### Création des partitions sur le disque raid c : "
# echo -n "# Création partition 1   : " ; sgdisk --new=1:2048:5860320000 /dev/sdc
# echo -n "# renommé partition      : " ; sgdisk --change-name=1:RAID3 /dev/sdc
# echo -n "# type GNU/Linux raid    : " ; sgdisk --typecode=1:FD00 /dev/sdc

# echo -e "\n### Création des partitions sur le disque raid d : "
# echo -n "# Création partition 1   : " ; sgdisk --new=1:2048:5860320000 /dev/sdd
# echo -n "# renommé partition      : " ; sgdisk --change-name=1:RAID4 /dev/sdd
# echo -n "# type GNU/Linux raid    : " ; sgdisk --typecode=1:FD00 /dev/sdd

# echo "<q> pour quiter, <enter> pour continuer"; read a ; [[ "$a" == "q" ]] && exit 1
# echo -e "\n### Affichage du résulta"
# for i in a b c d e ; do echo -e "\n\n### Disque /dev/sd${i}"; gdisk -l /dev/sd${i} ; done | tee CreaTable.txt
# less CreaTable.txt
