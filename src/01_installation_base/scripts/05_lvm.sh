# echo -e "\n### Création des volumes physiques"
# pvcreate /dev/sde3
# pvcreate /dev/md0
# echo "<q> pour quiter, <enter> pour continuer"; read a ; [ "$a" == "q" ] && exit 1
# pvdisplay | less

# echo -e "\n### Création des groupes de volumes"
# vgcreate vgs /dev/sde3
# vgcreate vgr /dev/md0
# echo "<q> pour quiter, <enter> pour continuer"; read a ; [ "$a" == "q" ] && exit 1
# vgdisplay | less

# echo -e "\n### Création des volumes logiques (partitions finales)"
# echo -n "# Système sur le SSD         : "; lvcreate -L  20G vgs -n syss
# echo -n "# Système sur le RAID (/var) : "; lvcreate -L  20G vgr -n sysr
# echo -n "# Utilisateur sur le RAID    : "; lvcreate -L 200G vgr -n docs
# echo -n "# Media sur le RAID          : "; lvcreate -L   6T vgr -n mdia
# echo "<q> pour quiter, <enter> pour continuer"; read a ; [ "$a" == "q" ] && exit 1
# lvdisplay | less

