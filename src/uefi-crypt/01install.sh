#!/bin/bash
#<1 script de préparation à l'installation.

#<2 Configuration bash
# termine le script en cas d'une erreur quelconque.
set -o errexit

# Fait échouer l’intégralité d’un pipeline en cas d’erreur
set -o pipefail

# Force le script à sortir en cas d’utilisation d’une variable non définie
set -o nounset


#<2 Varibales
printf "# Le disque d'installation est : ${disque:=/dev/sda}\n" ; readonly disque
printf "# Nom du montage du disque chiffré : ${DISKCHIF:=sda2c}\n" ; readonly DISKCHIF
printf "# Nom du volume de groupe LVM : ${VGCHIF:=vgarch}\n" ; readonly vgarch
printf "# Taille de la partition root : ${ROOTT:=30G}\n" ; readonly ROOTT
printf "# Taille de la partition home : ${HOMET:=30G}\n" ; readonly HOMET

#<1 table de partition
#<2 effacement
printf "> Effacement des tables de partitions :\n"
read -p "? Voulez vous vraiment effacer tout le contenu du disque ${disque} ? (Tapez oui en majuscule pour le faire) : " EFACCER
if [[ "${EFACCER}" = "OUI" ]] ; then
	printf "\n> Détruit les table de partition GPT et MBR :\n\t"
	sgdisk --zap-all ${disque}
	printf "\n> De même, avec une autre commande... Pour être sur que ce soit bien fait! :\n\t"
	sgdisk --clear ${disque}
fi

#<2 création
[[ "${EFACCER}" = "OUI" ]] && \
		read -p "? Voulez vous partitionner le disque ${disque} ? (Tapez oui en majuscule pour le faire) : " PARTITIONNER
if [[ "${PARTITIONNER}" = "OUI" ]] ; then
	printf "\n\n> Partition UEFI : "
	sgdisk --new=1:2048:616447 --change-name=1:UEFI --typecode=1:EF00 --attributes=1:set:2 ${disque}
	printf "\n\n> Partition Disque chifré : "
	#sgdisk --change-name=2:CRYPT --typecode=2:8300 --new=2:616448:234441614 ${disque}
	sgdisk --largest-new=2 --change-name=2:CRYPT --typecode=2:8300 ${disque}
fi

#<2 contrôle et informations
printf "\n> Le partitionnement du disque est le suivant : \n" | tee sgdisk.log
sgdisk -p ${disque} | tee -a sgdisk.log

printf "\n> Vérification du partitionnement :" | tee -a sgdisk.log
sgdisk -v ${disque} | tee -a sgdisk.log

printf "\n> Information sur les partition :" | tee -a sgdisk.log
printf "\n -i 1\n" ; sgdisk -i 1 ${disque} | tee -a sgdisk.log
printf "\n -i 2\n" ; sgdisk -i 2 ${disque} | tee -a sgdisk.log

printf "\n> Enregistrement d\'une sauvegarde de la table de partition dans sgdisk.backup"
sgdisk --backup=sgdisk.bakup ${disque}

ls -lh sgdisk.*

#<1 Chiffrement
printf "\n# Chiffrement avec LUKS\n"
printf "! Dans un premier temps, entrez une clé qui passe aussi bien\n"
printf "! sur un clavier anglais ou français (romand)\n"
# TODO Insérer le mot de passe une deuxième fois dans un nouveau slot avec le clavier US.
printf "\n> création du container chiffré : \n"
cryptsetup -c aes-xts-plain64 -y --hash sha512 --use-random luksFormat ${disque}2

printf "\nOuverture du container : \n"
cryptsetup luksOpen ${disque}2 ${DISKCHIF}


#<1 LVM, partitionnement et formatage
printf "\n> Formatage de la partition UEFI : "
mkfs.vfat -F32 -n UEFI ${disque}1

printf "\n# Mise en place de lvm"
printf "\n> Création du volume physique : "
pvcreate /dev/mapper/${DISKCHIF}

printf "\n> Création du volume de groupe : "
vgcreate ${VGCHIF} /dev/mapper/${DISKCHIF}

printf "\n> Partition Root : "
printf "\n>\tCréation : "
lvcreate --size ${ROOTT} ${VGCHIF} --name root

printf "\n>\tFormatage : "
mkfs.ext4 -L ROOT /dev/mapper/${VGCHIF}-root


printf "\n> Partition home : "
printf "\n>\tCréation : "
lvcreate --size ${HOMET} ${VGCHIF} --name home

printf "\n>\tFormatage : "
mkfs.ext4 -L HOME /dev/mapper/${VGCHIF}-home


#<1 Préparation de l'installation
printf "\n# Montage des partitions"
printf "\n> Racine : "
mount /dev/mapper/${VGCHIF}-root /mnt

printf "\n> Création des répertoires de montage : "
mkdir -vp /mnt/{home,boot/efi}

printf "\n> UEFI : "
mount ${disque}1 /mnt/boot/efi

printf "\n> home : "
mount /dev/mapper/${VGCHIF}-home /mnt/home


printf "\n#<1 Installation du système de base"
printf "\n> pacstrap : "
pacstrap /mnt base base-devel grub-efi-x86_64 git efibootmgr mtools dosfstools exfat-utils linux-lts zsh zsh-completions vim vim-spell-fr vim-nerdtree ranger tree 

printf "\n> Génération du fstab : "
genfstab -pU /mnt >> /mnt/etc/fstab

exit 0
#<1 vim: set ts=3 sts=3 sw=3 tw=100 fdm=marker foldmarker=#<,#> filetype=sh spell:%
