Ce que le projet fait
=====================

Tenue d'un journal d'installation et de maintenance d'un serveur de fichiers
et d'applications sous ArchLinux


Pour qui il est fait
====================

Strictement privé.
Comme ce n'est pas confidentiel, tout le monde peut y accéder. Sait-on jamais,
cela pourrait peu-être être utile à quelqu'un...


Les près-requis matériels et logiciels
======================================

Matériels
---------
Aucun, cela devrait fonctionner sur toute machine faisant tourner un GNU/Linux


Logiciels
---------
+ txt2tags
  - python
+ git
  - git flow
+ make
+ vim


La procédure d'installation
===========================

Il suffit de cloner le dossier.
Cela //doit// fonctionner sur n'importe quel GNU/Linux sans aucun problème.
- Tous les prérequis sont disponibles dans les dépôts de toutes les distributions bureautiques.
- Si vous êtes sur Mac OS... Mauvaise idée... Votre punition sera de vous débrouiller tout seul.
- Si vous sous Windows, all... La décence m'oblige à la censure du fond de ma pensée.



Localisation de la documentation
================================

Documentation du projet
-----------------------
La documentation propre à ce projet se trouve dans le dossier **doc**


Pour les logiciels nécessaires
------------------------------
### txt2tags
 [→](http://fr.wikipedia.org/wiki/Txt2tags)
  La page de Wikipédia pour une idée générale
- [→](http://txt2tags.org)
  Le site officiel
- [→](http://txt2tags.org/fr/)
  Le dossier contenant toute la documentation officielle en français
- [→](http://fr.openclassrooms.com/informatique/cours/creer-des-documents-avec-txt2tags)
  Le cours du site du zéro
- [→](http://l.rnv.ch/rc/?searchtags=txt2tags)
  Une liste de mes liens sur txt2tags


### Git
- [→](http://fr.wikipedia.org/wiki/Git)
  La page de Wikipédia pour se faire une idée
- [→](http://git-scm.com/)
  Site officiel
- [→](http://djibril.developpez.com/tutoriels/conception/pro-git/)
  Un livre en ligne permettant de connaître git sur le bout des doigts
- [→](http://jeromesmadja.github.io/2013/03/23/git-flow.html)
  L'utilisation du module git flow utilisé ici
- [→](http://l.rnv.ch/rc/?searchtags=git)
  Une liste de mes liens sur Git


### make
- [→](http://fr.wikipedia.org/wiki/Make)
  La page de Wikipédia pour se faire une idée
- [→](file:///home/rc/.scrapbook/doc-info/data/20070829120322/index.html)
  Une introduction assez bien faite.
- [→](http://graal.ens-lyon.fr/~fvivien/Enseignement/PPP-2001-2002/TP03-GNUmake/index.html)
  Une explication, ancienne, mais toujours d'actualité, sur Make. C'est du lourd, c'est complet!


### Vim
- [→](http://fr.wikipedia.org/wiki/Vim)
  La page wikipédia pour se faire une idée
- [→](http://geekblog.over-blog.com/article-5884386.html)
  Une introduction basique
- [→](http://ftp.traduc.org/doc-vf/gazette-linux/html/2008/152/lg152-C.html)
  Une introduction un petit peut plus poussée
- [→](http://www.metal3d.org/ticket/2008/12/08/vim-est-un-IDE-PHP)
  Quelques plus de vim
- [→](http://l.rnv.ch/rc/?searchtags=vim)
  Une liste de mes liens


### lftp
- [→](http://fr.wikipedia.org/wiki/Lftp)
  La page Wikipédia pour se faire une idée
- [→](http://doc.ubuntu-fr.org/lftp)
  Pour une première utilisation


